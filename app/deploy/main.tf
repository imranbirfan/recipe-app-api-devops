terraform {
  backend "s3" {
    bucket         = "iirfan-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-tfstate-lock"
  }
  #required_version = ">= 0.12"
}

provider "aws" {
  region  = "us-west-2"
  version = "~> 2.54.0"
}

# Locals are local variables that are used within the TF script
# Allows for the creation of dynamic variables
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  # The above uses string interpolation to grab a variable from the variables.tf file under the "variables block"
  # called "prefix and interpolates its value here. The "terraform.workspace" variable is a predefined variable used 
  # by terraform
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}